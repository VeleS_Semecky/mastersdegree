package com.example.roomdagger2mvvm2.core;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.roomdagger2mvvm2.dao.DAO;
import com.example.roomdagger2mvvm2.model.Name;

@Database(entities = {Name.class}, version = 1, exportSchema = false)
//TODO: ADD Your DAO
public abstract class AppRoomDatabase extends RoomDatabase {
    public abstract DAO dao();
}
