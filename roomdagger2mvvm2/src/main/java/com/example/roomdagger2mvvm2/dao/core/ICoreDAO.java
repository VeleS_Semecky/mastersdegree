package com.example.roomdagger2mvvm2.dao.core;

import java.util.List;

public interface ICoreDAO {
    interface Delete {
        void delete(int count);
    }
    interface InsertAll {
        void insertAll(List<Long> count);
    }
    interface InsertOne {
        void insertOne(Long count);
    }
    interface Update {
        void update(int count);
    }
}
