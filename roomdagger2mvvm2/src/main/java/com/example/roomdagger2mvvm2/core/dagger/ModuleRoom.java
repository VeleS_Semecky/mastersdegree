package com.example.roomdagger2mvvm2.core.dagger;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.roomdagger2mvvm2.Repository;
import com.example.roomdagger2mvvm2.core.AppRoomDatabase;
import com.example.roomdagger2mvvm2.dao.DAO;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModuleRoom {
    private AppRoomDatabase appRoomDatabase;

    public ModuleRoom(Application mApplication, String name) {
        appRoomDatabase = Room.databaseBuilder(mApplication, AppRoomDatabase.class, name).build();
    }

    @Singleton
    @Provides
    AppRoomDatabase providesRoomDatabase() {
        return appRoomDatabase;
    }

    //TODO:Change DAO
    @Singleton
    @Provides
    DAO providesProductDao(AppRoomDatabase appRoomDatabase) {
        return appRoomDatabase.dao();
    }
    //TODO:Change DAO
    @Singleton
    @Provides
    Repository productRepository(DAO productDao) {
        return new Repository(productDao);
    }
}
