package com.example.yurii.mastersdegree.api.model.queryautocomplete.core;

import com.example.yurii.mastersdegree.api.model.queryautocomplete.Predictions;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HeadPlace {
    @SerializedName("predictions")
    @Expose
    private List<Predictions> predictions = null;

    @SerializedName("status")
    @Expose
    private String status;

    public List<Predictions> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<Predictions> predictions) {
        this.predictions = predictions;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
