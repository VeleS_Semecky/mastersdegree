package com.example.yurii.mastersdegree.ui.navigator;


import com.example.yurii.mastersdegree.R;
import com.example.yurii.mastersdegree.ui.activity.core.BaseActivity;
import com.example.yurii.mastersdegree.ui.fragment.FirstFragment_;
import com.example.yurii.mastersdegree.ui.fragment.core.BaseFragment;
import com.example.yurii.mastersdegree.ui.navigator.core.BaseManager;
import com.example.yurii.mastersdegree.ui.navigator.core.ResourceManager;
import com.example.yurii.mastersdegree.ui.navigator.core.ResourceNames;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ManagerMain extends BaseManager {

    ManagerMain(BaseFragment baseFragment, BaseActivity baseActivity) {
        super(baseFragment, baseActivity);
    }

    @Override
    public void moveFragmentTo(int id, Object... o) {
        switch (id){
            case ResourceManager.FragmentId.FIRST_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerToolbar, FirstFragment_.builder().build(), ResourceNames.FIRST_FRAGMENT).commit();
                break;
        }
    }

    @Override
    public void removeFragment() {
        if (baseFragment.getActivity()!=null)
            baseFragment.getActivity().getFragmentManager().beginTransaction().remove(baseFragment).commit();
    }
}
