package com.example.yurii.mastersdegree.api.networking;

import com.example.yurii.mastersdegree.R;
import com.example.yurii.mastersdegree.api.model.queryautocomplete.core.HeadPlace;
import com.example.yurii.mastersdegree.api.networking.core.Service;
import com.example.yurii.mastersdegree.api.networking.error.NetworkError;
import com.example.yurii.mastersdegree.api.service.QueryAutoCompleteAPI;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class NetworkQueryAutoComplete extends Service<QueryAutoCompleteAPI> {
    public NetworkQueryAutoComplete(QueryAutoCompleteAPI networkService) {
        super(networkService);
    }

    public Disposable getHeadPlace(String input,Callback<HeadPlace> headCallback) {
        return networkService.getHeadPlace(input,"AIzaSyAg3g_tlpXkqzXXgxcfiwCFi50s_JcbLBk")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .onErrorResumeNext(Observable::error)
                .subscribeWith(new DisposableObserver<HeadPlace>() {
                                   @Override
                                   public void onNext(HeadPlace headPlace) {
                                       headCallback.onSuccess(headPlace);
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       headCallback.onError(new NetworkError(e));
                                   }

                                   @Override
                                   public void onComplete() {
                                   }
                               }
                );
    }
}
