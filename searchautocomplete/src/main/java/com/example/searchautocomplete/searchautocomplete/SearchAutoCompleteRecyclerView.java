package com.example.searchautocomplete.searchautocomplete;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.SearchView;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;

import com.example.searchautocomplete.R;
import com.example.searchautocomplete.adapter.bean.ProviderBeanField;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

@EBean
public class SearchAutoCompleteRecyclerView implements SearchView.OnQueryTextListener {
    @RootContext
    protected Context baseActivity;

    private SearchView searchView;

    @ViewById(resName = "thpb")
    protected ProgressBar progressBar;

    @Bean
    protected ProviderBeanField providerBeanField;

    private IOnQueryTextListener onQueryTextListener;

    public void init(SearchView searchView){
        this.searchView = searchView;
    }

    public void setSetting(){
        searchView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        searchView.setQueryHint("Search");
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(this);
        progressBar.setForeground(baseActivity.getDrawable(R.color.colorPrimary));

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean newViewFocus) {
                if (!newViewFocus) {
                    offProgressBar();
                    providerBeanField.clearAllItem();
                }
            }
        });
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public ProviderBeanField getProviderBeanField() {
        return providerBeanField;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        onQueryTextListener.onIQueryTextSubmit(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 0) {
            onProgressBar();
        }else {
            offProgressBar();
            providerBeanField.clearAllItem();
        }
        onQueryTextListener.onIQueryTextChange(newText);
        return false;
    }


    public void setOnQueryTextListener(IOnQueryTextListener onQueryTextListener) {
        this.onQueryTextListener = onQueryTextListener;
    }

    public void offProgressBar(){
        progressBar.setIndeterminate(false);
        progressBar.setForeground(baseActivity.getDrawable(R.color.colorPrimary));
    }

    public void onProgressBar(){
        progressBar.setIndeterminate(true);
        progressBar.setForeground(null);

    }
}
