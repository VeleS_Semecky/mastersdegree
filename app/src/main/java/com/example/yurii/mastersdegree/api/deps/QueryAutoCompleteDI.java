package com.example.yurii.mastersdegree.api.deps;

import com.example.yurii.mastersdegree.api.bean.BeanQueryAutoComplete;
import com.example.yurii.mastersdegree.api.networking.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class})
public interface QueryAutoCompleteDI {
    void inject(BeanQueryAutoComplete beanQueryAutoComplete);

}
