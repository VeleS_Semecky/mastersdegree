package com.example.yurii.mastersdegree.maps;

import com.google.android.gms.location.LocationResult;

public interface ILocationCallback {
   void onLocationResult(LocationResult locationResult);
}
