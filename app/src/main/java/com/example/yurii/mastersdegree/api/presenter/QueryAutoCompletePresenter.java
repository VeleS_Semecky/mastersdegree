package com.example.yurii.mastersdegree.api.presenter;

import com.example.yurii.mastersdegree.api.core.BaseView;
import com.example.yurii.mastersdegree.api.model.queryautocomplete.core.HeadPlace;
import com.example.yurii.mastersdegree.api.networking.NetworkQueryAutoComplete;
import com.example.yurii.mastersdegree.api.networking.core.Service;
import com.example.yurii.mastersdegree.api.networking.error.NetworkError;
import com.example.yurii.mastersdegree.api.presenter.core.Presenter;

public class QueryAutoCompletePresenter extends Presenter<NetworkQueryAutoComplete,HeadPlace> {
    public QueryAutoCompletePresenter(NetworkQueryAutoComplete service, BaseView<HeadPlace> headPlaceBaseView) {
        super(service, headPlaceBaseView);
    }
    public void getQueryAutoComplete(String input){
        yBaseView.showWait();
        subscription.add(service.getHeadPlace(input ,new Service.Callback<HeadPlace>() {
            @Override
            public void onSuccess(HeadPlace headPlace) {
                yBaseView.closeWait();
                yBaseView.onSuccess(headPlace);
                onStop();
            }

            @Override
            public void onError(NetworkError networkError) {
                yBaseView.closeWait();
                yBaseView.onFailed(networkError.getAppErrorMessage());//
                onStop();
            }
        }));
        super.subscription.add(subscription);
    }
}
