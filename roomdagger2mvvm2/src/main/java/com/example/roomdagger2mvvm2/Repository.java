package com.example.roomdagger2mvvm2;


import com.example.roomdagger2mvvm2.core.BaseRepository;
import com.example.roomdagger2mvvm2.dao.DAO;
import com.example.roomdagger2mvvm2.model.Name;


import javax.inject.Inject;
//TODO:Change DAO and TABLE
public class Repository extends BaseRepository<Name> {

    public DAO getDao() {
        return mDao;
    }

    private DAO mDao;

    @Inject
    public Repository(DAO DAO){
        super(DAO);
        this.mDao = DAO;
    }

}
