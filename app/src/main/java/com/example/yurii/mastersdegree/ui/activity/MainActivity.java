package com.example.yurii.mastersdegree.ui.activity;

import android.view.Window;


import com.example.yurii.mastersdegree.R;
import com.example.yurii.mastersdegree.ui.activity.core.BaseActivity;
import com.example.yurii.mastersdegree.ui.navigator.NavigatorManager;
import com.example.yurii.mastersdegree.ui.navigator.core.ResourceManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.WindowFeature;



@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    @Bean
    public NavigatorManager navigatorManager;
    @InstanceState
    Integer myInstanceState = 0;
    @AfterViews
    public void initView(){
        if(myInstanceState == 0) {

            navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.FIRST_FRAGMENT);
            myInstanceState = 1;

        }
    }

}
