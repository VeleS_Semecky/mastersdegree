package com.example.yurii.mastersdegree.api.presenter.core;


import com.example.yurii.mastersdegree.api.core.BaseView;

import io.reactivex.disposables.CompositeDisposable;

public abstract class Presenter<T,Y> {

    protected final T service;
    protected final BaseView<Y> yBaseView;
    protected CompositeDisposable subscription;

    public Presenter(T service, BaseView<Y> yBaseView) {
        this.service = service;
        this.yBaseView = yBaseView;
        this.subscription = new CompositeDisposable();
    }

    protected void onStop(){
        subscription.dispose();
    }
}
