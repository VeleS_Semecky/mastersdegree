package com.example.yurii.mastersdegree.api.service;

import com.example.yurii.mastersdegree.api.model.RouteResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SongApi{


    @GET("/maps/api/directions/json")
    Observable<RouteResponse> getRoute(
            @Query(value = "origin") String position //"48.918966,24.715521"
            ,@Query(value = "destination") String destination//48.919015,24.718160
            ,@Query(value = "key") String key//AIzaSyBYlJ6kFEYDFLH5BfrnY56tBF2oo3rtA7U
    );

}
