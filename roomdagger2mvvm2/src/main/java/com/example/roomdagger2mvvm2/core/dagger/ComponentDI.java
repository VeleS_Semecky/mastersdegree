package com.example.roomdagger2mvvm2.core.dagger;

import android.app.Application;

import com.example.roomdagger2mvvm2.ApplicationViewModel;
import com.example.roomdagger2mvvm2.Repository;
import com.example.roomdagger2mvvm2.core.AppRoomDatabase;
import com.example.roomdagger2mvvm2.dao.DAO;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(dependencies = {}, modules = {AppModule.class, ModuleRoom.class})
public interface ComponentDI {
    void inject(ApplicationViewModel mainActivity);

    //TODO: ADD Your DAO
    DAO dao();
    AppRoomDatabase appRoomDatabase();
    Repository thRepository();
    Application thApp();
}
