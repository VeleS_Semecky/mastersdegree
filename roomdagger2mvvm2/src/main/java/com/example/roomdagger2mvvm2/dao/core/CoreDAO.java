package com.example.roomdagger2mvvm2.dao.core;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import com.example.roomdagger2mvvm2.constant.RoomConstant;

import java.util.List;
public interface CoreDAO<T> {

    @Delete
    int delete(T t);

    @Delete
    int delete(List<T> t);

    @Insert(onConflict = RoomConstant.onConflictDaoInsert)
    List<Long> insertAll(List<T> t);

    @Insert(onConflict = RoomConstant.onConflictDaoInsert)
    long insertOne(T t);

    @Update
    int update(T t);

    @Update
    int update(List<T> t);

}
