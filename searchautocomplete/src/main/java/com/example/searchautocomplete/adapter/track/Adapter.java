package com.example.searchautocomplete.adapter.track;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.searchautocomplete.adapter.model.ObGJer;
import com.example.searchautocomplete.adapter.bean.finder.AddFieldInAdapter;
import com.example.searchautocomplete.adapter.core.RecyclerViewAdapterBase;
import com.example.searchautocomplete.adapter.core.ViewWrapper;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;


@EBean
public class Adapter extends RecyclerViewAdapterBase<ObGJer, ItemView> {
    @RootContext
    protected Context baseActivity;

    @Bean
    AddFieldInAdapter addFieldInAdapter;


    @AfterViews
    void initAdapter(){
//        setItem(addFieldInAdapter.obGJers());
        // Get a new or existing ViewModel from the ViewModelProvider.
        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
//        trackExoFinder.startService();
//        trackBeanFinder.prepareExoPlayerFromFileUri("");
    }

    @Override
    protected ItemView onCreateItemHolder(ViewGroup parent, int viewType) {
        return ItemView_.build(baseActivity);
    }



//    @Override
//    public void onBindViewHolder(@NonNull ViewWrapper<FieldItemView> holder, int position) {
//
//        FieldItemView view = holder.getView();
//        Track track = getItem().get(position);
//        view.bind(track);
////        view.setPlayerInItem(trackExoFinder);
//        view.setTag(track);
////                view.setOnClickListener(v -> trackExoFinder.playMysic());
//
////        view.setOnClickListener(v -> { trackBeanFinder.prepareExoPlayerFromFileUri(trackStart.getName());});
////        view.setOnClickListener(v -> trackExoFinder.startMyService(trackStart.getName()));


    @Override
    public void onBindViewHolder(@NonNull ViewWrapper<ItemView> holder, int position) {
        ItemView view = holder.getView();
        ObGJer fieldTable = getItem().get(position);
        view.bind(fieldTable);
        view.setTag(position);
        setAnimation(holder.itemView, position);
//        ProgressDialogInMyApp progressDialogInMyApp = new ProgressDialogInMyApp(baseActivity,fieldTable.getNameField());
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                progressDialogInMyApp.showDialog();
//            }
//        });
    }
    private int lastPosition = -1;
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
//            Animation animation = AnimationUtils.loadAnimation(baseActivity, android.R.anim.slide_in_left);
//            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
