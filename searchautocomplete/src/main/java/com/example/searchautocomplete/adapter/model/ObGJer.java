package com.example.searchautocomplete.adapter.model;

public class ObGJer {
    private String field;
    private  Object object;

    public ObGJer() {
    }

    public ObGJer(String field, Object object) {
        this.field = field;
        this.object = object;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
