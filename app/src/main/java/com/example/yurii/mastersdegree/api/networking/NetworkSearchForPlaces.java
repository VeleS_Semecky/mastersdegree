package com.example.yurii.mastersdegree.api.networking;

import com.example.yurii.mastersdegree.api.model.queryautocomplete.core.HeadPlace;
import com.example.yurii.mastersdegree.api.model.searchforplaces.core.SearchForPlaces;
import com.example.yurii.mastersdegree.api.networking.core.Service;
import com.example.yurii.mastersdegree.api.networking.error.NetworkError;
import com.example.yurii.mastersdegree.api.service.SearchForPlacesAPI;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class NetworkSearchForPlaces extends Service<SearchForPlacesAPI>  {
    public NetworkSearchForPlaces(SearchForPlacesAPI networkService) {
        super(networkService);
    }
    public Disposable getSearchForPlaces(String mode, String query, String key, Callback<SearchForPlaces> headCallback) {
        return networkService.getSearchForPlaces("mapbox.places",query,"pk.eyJ1IjoidmVsZXNzZW1lY2t5IiwiYSI6ImNqaW5sYmNlaTBnN2kzdm1oZmFkZWNkcWwifQ.w6iZpP4GPvw6efKXiz3f5Q")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .onErrorResumeNext(Observable::error)
                .subscribeWith(new DisposableObserver<SearchForPlaces>() {
                                   @Override
                                   public void onNext(SearchForPlaces t) {
                                       headCallback.onSuccess(t);
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       headCallback.onError(new NetworkError(e));
                                   }

                                   @Override
                                   public void onComplete() {
                                   }
                               }
                );
    }
}
