package com.example.searchautocomplete.adapter.core;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class RecyclerViewAdapterBase<T,V extends View> extends RecyclerView.Adapter<ViewWrapper<V>>{

    private List<T> item = new ArrayList<>();

    public void setItem(List<T> item){
        this.item = item;
        notifyDataSetChanged();
    }

    public List<T> getItem() {
        return item;
    }

    @Override
    public int getItemCount() {
        return item == null ? 0 : item.size();
    }

    @NonNull
    @Override
    public ViewWrapper<V> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewWrapper<>(onCreateItemHolder(parent, viewType));
    }

    protected abstract V onCreateItemHolder(ViewGroup parent, int viewType);

}
