package com.example.roomdagger2mvvm2.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;


import com.example.roomdagger2mvvm2.constant.RoomConstant;
import com.example.roomdagger2mvvm2.dao.core.CoreDAO;
import com.example.roomdagger2mvvm2.model.Name;

import java.util.List;

@Dao
public interface DAO extends CoreDAO<Name> {
    //TODO:ADD Your QUERY and Name TABLE
    @Query("SELECT * FROM " + RoomConstant.TableNameRoomDatabase.TABLE_NAMES)
    LiveData<List<Name>> getAllNames();

    @Query("SELECT * FROM " + RoomConstant.TableNameRoomDatabase.TABLE_NAMES + " WHERE id = :thID")
    LiveData<Name> getSearchById(String thID);


    @Query("SELECT * FROM " + RoomConstant.TableNameRoomDatabase.TABLE_NAMES + " WHERE name =:thNAme")
    LiveData<Name> getSearchByName(String thNAme);

}
