package com.example.yurii.mastersdegree.api.networking;


import com.example.yurii.mastersdegree.api.model.RouteResponse;
import com.example.yurii.mastersdegree.api.networking.core.Service;
import com.example.yurii.mastersdegree.api.networking.error.NetworkError;
import com.example.yurii.mastersdegree.api.service.SongApi;

import java.io.IOException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class NetworkSong extends Service<SongApi> {
    public NetworkSong(SongApi networkService) {
        super(networkService);
    }


    public Disposable getRoute(Callback<RouteResponse> headCallback) {
        return networkService.getRoute("48.918966,24.715521", "48.919015,24.718160", "AIzaSyDYf31R30IwYMMYMX-lNXio6RW746cPi5k")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .onErrorResumeNext(Observable::error)
                .subscribeWith(new DisposableObserver<RouteResponse>() {
                                   @Override
                                   public void onNext(RouteResponse routeResponse) {
                                       headCallback.onSuccess(routeResponse);
                                   }

                                   @Override
                                   public void onError(Throwable e) {
                                       headCallback.onError(new NetworkError(e));
                                   }

                                   @Override
                                   public void onComplete() {
                                   }
                               }
                );
    }

}
