package com.example.searchautocomplete.adapter.bean.finder;





import com.example.searchautocomplete.adapter.model.ObGJer;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

@EBean
public class AddFieldInAdapter {


    public List<ObGJer> obGJers(){
        List<ObGJer> obGJers = new ArrayList<>();
        obGJers.add(new ObGJer("aaaa","aaa"));
        obGJers.add(new ObGJer("aaaa","aaa"));
        obGJers.add(new ObGJer("aaaa","aaa"));
        obGJers.add(new ObGJer("aaaa","aaa"));
        obGJers.add(new ObGJer("aaaa","aaa"));
        return obGJers;
    }

//    public List<DepartmentTable> setDepartmentTables(){
//        List<DepartmentTable> departmentTables = new ArrayList<>();
////        for (int i=0;i<5;i++) {
////            DepartmentTable departmentTable = new DepartmentTable("name" + i);
////            departmentTables.add(departmentTable);
////        }
//        DepartmentTable departmentTable = new DepartmentTable("Відділ Покупок");
//        DepartmentTable departmentTable1 = new DepartmentTable("Відділ БДСМ");
//        DepartmentTable departmentTable2 = new DepartmentTable("Відділ Андроїд");
//        DepartmentTable departmentTable3 = new DepartmentTable("Відділ Алкоголю");
//        departmentTables.add(departmentTable);
//        departmentTables.add(departmentTable1);
//        departmentTables.add(departmentTable2);
//        departmentTables.add(departmentTable3);
//        return departmentTables;
//    }
//
//    public List<FieldTable> setFieldTables(){
//        List<FieldTable> fieldTables = new ArrayList<>();
//        FieldTable fieldTable = new FieldTable("Вхід_Покупок","Відділ Покупок");
//        FieldTable fieldTable1 = new FieldTable("Вихід_Покупок","Відділ Покупок");
//        FieldTable fieldTable2 = new FieldTable("ЗаднійПрохід_Покупок","Відділ Покупок");
//        fieldTables.add(fieldTable);
//        fieldTables.add(fieldTable1);
//        fieldTables.add(fieldTable2);
//        FieldTable fieldTable11 = new FieldTable("ВхідБДСМ","Відділ БДСМ");
//        FieldTable fieldTable12 = new FieldTable("ВихідБДСМ","Відділ БДСМ");
//        FieldTable fieldTable23 = new FieldTable("ЗаднійПрохідБДСМ","Відділ БДСМ");
//        fieldTables.add(fieldTable11);
//        fieldTables.add(fieldTable12);
//        fieldTables.add(fieldTable23);
//        FieldTable fieldTable112 = new FieldTable("ВхідАндроїд","Відділ Андроїд");
//        FieldTable fieldTable122 = new FieldTable("ВихідАндроїд","Відділ Андроїд");
//        FieldTable fieldTable232 = new FieldTable("ЗаднійПрохідАндроїд","Відділ Андроїд");
//        fieldTables.add(fieldTable112);
//        fieldTables.add(fieldTable122);
//        fieldTables.add(fieldTable232);
//        FieldTable fieldTable1123 = new FieldTable("ВхідАлкоголю","Відділ Алкоголю");
//        FieldTable fieldTable1223 = new FieldTable("ВихідАлкоголю","Відділ Алкоголю");
//        FieldTable fieldTable2323 = new FieldTable("ЗаднійПрохідАлкоголю","Відділ Алкоголю");
//        fieldTables.add(fieldTable1123);
//        fieldTables.add(fieldTable1223);
//        fieldTables.add(fieldTable2323);
//
////        for (int i=0;i<5;i++) {
////            FieldTable fieldTable = new FieldTable("nameDP" + i,"name" + 1);
////            fieldTables.add(fieldTable);
////            FieldTable fieldTable1 = new FieldTable("nameDP1" + i,"name" + 2);
////            fieldTables.add(fieldTable1);
////            FieldTable fieldTable2 = new FieldTable("nameDP2" + i,"name" + 3);
////            fieldTables.add(fieldTable2);
////            FieldTable fieldTable3 = new FieldTable("nameDP3" + i,"name" + 0);
////            fieldTables.add(fieldTable3);
////            FieldTable fieldTable4 = new FieldTable("nameDP4" + i,"name" + 4);
////            fieldTables.add(fieldTable4);
////        }
////        for (int i=0;i<5;i++) {
////
////        }
////        for (int i=0;i<5;i++) {
////
////        }
////        for (int i=0;i<5;i++) {
////
////        }
////        for (int i=0;i<5;i++) {
////
////        }
//        return fieldTables;
//    }
//
//    public List<PolygonTable> setPolygonTables(){
//        List<PolygonTable> polygonTables = new ArrayList<>();
//        for (int i=0;i<11;i++) {
//          ArrayList<String> strings = new ArrayList<>();
//                    strings.add("Вхід_Покупок");
//                    strings.add("Вихід_Покупок");
//                    strings.add("ЗаднійПрохід_Покупок");
//
//                    strings.add("ВхідБДСМ");
//                    strings.add("ВихідБДСМ");
//                    strings.add("ЗаднійПрохідБДСМ");
//
//                    strings.add("ВхідАндроїд");
//                    strings.add("ВихідАндроїд");
////                    strings.add("ЗаднійПрохідАндроїд");
//
//                    strings.add("ВхідАлкоголю");
//                    strings.add("ВихідАлкоголю");
//                    strings.add("ЗаднійПрохідАлкоголю");
//
//            PolygonTable polygonTable1 = new PolygonTable(strings.get(i), 45.522585, -122.685699);
//            PolygonTable polygonTable2 = new PolygonTable(strings.get(i), 45.534611, -122.708873);
//            PolygonTable polygonTable3 = new PolygonTable(strings.get(i), 45.530883, -122.678833);
//            PolygonTable polygonTable4 = new PolygonTable(strings.get(i), 45.547115, -122.667503);
//            PolygonTable polygonTable5 = new PolygonTable(strings.get(i), 45.530643, -122.660121);
//            PolygonTable polygonTable6 = new PolygonTable(strings.get(i), 45.533529, -122.636260);
//            PolygonTable polygonTable7 = new PolygonTable(strings.get(i), 45.521743, -122.659091);
//            PolygonTable polygonTable8 = new PolygonTable(strings.get(i), 45.510677, -122.648792);
//            PolygonTable polygonTable9 = new PolygonTable(strings.get(i), 45.515008, -122.664070);
//            PolygonTable polygonTable10 = new PolygonTable(strings.get(i), 45.502496, -122.669048);
//            PolygonTable polygonTable11 = new PolygonTable(strings.get(i), 45.515369, -122.678489);
//            PolygonTable polygonTable12 = new PolygonTable(strings.get(i), 45.522585, -122.685699);
//            polygonTables.add(polygonTable1);
//            polygonTables.add(polygonTable2);
//            polygonTables.add(polygonTable3);
//            polygonTables.add(polygonTable4);
//            polygonTables.add(polygonTable5);
//            polygonTables.add(polygonTable6);
//            polygonTables.add(polygonTable7);
//            polygonTables.add(polygonTable8);
//            polygonTables.add(polygonTable9);
//            polygonTables.add(polygonTable10);
//            polygonTables.add(polygonTable11);
//            polygonTables.add(polygonTable12);
//        }
//
//
//
//            PolygonTable polygonTable1 = new PolygonTable("ЗаднійПрохідАндроїд", 45.622585, -122.785699);
//            PolygonTable polygonTable2 = new PolygonTable("ЗаднійПрохідАндроїд", 45.634611, -122.808873);
//            PolygonTable polygonTable3 = new PolygonTable("ЗаднійПрохідАндроїд", 45.630883, -122.778833);
//            PolygonTable polygonTable4 = new PolygonTable("ЗаднійПрохідАндроїд", 45.647115, -122.767503);
//            PolygonTable polygonTable5 = new PolygonTable("ЗаднійПрохідАндроїд", 45.630643, -122.760121);
//            PolygonTable polygonTable6 = new PolygonTable("ЗаднійПрохідАндроїд", 45.633529, -122.736260);
//            PolygonTable polygonTable7 = new PolygonTable("ЗаднійПрохідАндроїд", 45.621743, -122.759091);
//            PolygonTable polygonTable8 = new PolygonTable("ЗаднійПрохідАндроїд", 45.610677, -122.748792);
//            PolygonTable polygonTable9 = new PolygonTable("ЗаднійПрохідАндроїд", 45.615008, -122.764070);
//            PolygonTable polygonTable10 = new PolygonTable("ЗаднійПрохідАндроїд", 45.602496, -122.769048);
//            PolygonTable polygonTable11 = new PolygonTable("ЗаднійПрохідАндроїд", 45.615369, -122.778489);
//            PolygonTable polygonTable12 = new PolygonTable("ЗаднійПрохідАндроїд", 45.622585, -122.785699);
//            polygonTables.add(polygonTable1);
//            polygonTables.add(polygonTable2);
//            polygonTables.add(polygonTable3);
//            polygonTables.add(polygonTable4);
//            polygonTables.add(polygonTable5);
//            polygonTables.add(polygonTable6);
//            polygonTables.add(polygonTable7);
//            polygonTables.add(polygonTable8);
//            polygonTables.add(polygonTable9);
//            polygonTables.add(polygonTable10);
//            polygonTables.add(polygonTable11);
//            polygonTables.add(polygonTable12);
//
//        return polygonTables;
//    }



}
