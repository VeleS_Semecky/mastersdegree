package com.example.yurii.mastersdegree.api.bean.core;



import android.arch.lifecycle.MutableLiveData;
import android.content.Context;


import com.example.yurii.mastersdegree.api.core.BaseView;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import javax.inject.Inject;

@EBean
public abstract class Bean<T,Y> implements BaseView<T> {

    protected MutableLiveData<T> thLiveData = new MutableLiveData<>();

    public MutableLiveData<T> getThLiveData() {
        return thLiveData;
    }

    @Inject
    public Y networkInject;

    @Override
    public void onFailed(String error) {

    }

    @Override
    public void onSuccess(T t) {
        if (t!=null)
        thLiveData.setValue(t);

    }

    @Override
    public void showWait() {

    }

    @Override
    public void closeWait() {

    }

    @RootContext
    public Context baseActivity;




}
