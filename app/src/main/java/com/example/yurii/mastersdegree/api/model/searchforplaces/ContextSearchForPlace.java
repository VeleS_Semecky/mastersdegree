package com.example.yurii.mastersdegree.api.model.searchforplaces;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContextSearchForPlace {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("short_code")
    @Expose
    private String shortCode;
    @SerializedName("wikidata")
    @Expose
    private String wikidata;
    @SerializedName("text")
    @Expose
    private String text;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getWikidata() {
        return wikidata;
    }

    public void setWikidata(String wikidata) {
        this.wikidata = wikidata;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
