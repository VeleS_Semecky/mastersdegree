package com.example.yurii.mastersdegree.api.bean;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;
import android.widget.Toast;


import com.example.yurii.mastersdegree.api.bean.core.Bean;
import com.example.yurii.mastersdegree.api.deps.DaggerRouteResponseDI;
import com.example.yurii.mastersdegree.api.model.RouteResponse;
import com.example.yurii.mastersdegree.api.networking.NetworkSong;
import com.example.yurii.mastersdegree.api.networking.module.NetworkModule;
import com.example.yurii.mastersdegree.api.presenter.SongPresenter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EBean;

import java.io.File;

@EBean

public class BeanRouteResponsePresenter extends Bean<RouteResponse,NetworkSong> {

    @Override
    public MutableLiveData<RouteResponse> getThLiveData() {
        return super.getThLiveData();
    }

    @AfterViews
    void init() {
        DaggerRouteResponseDI.builder().networkModule(
                new NetworkModule(
                        new File(
                                baseActivity.getCacheDir(),
                                "responses"),"https://maps.googleapis.com")).build().inject(this);
    }

    @AfterViews
    void getSong() {
        new SongPresenter(networkInject, this).getRoute();
    }

    public void initGetSong() {
        new SongPresenter(networkInject, this).getRoute();
    }

    @Override
    public void onSuccess(RouteResponse song) {
        Log.i("GetServerData", song.getPoints());
        thLiveData.setValue(song);
        Toast.makeText(baseActivity, song.getPoints(), Toast.LENGTH_SHORT).show();
    }

}