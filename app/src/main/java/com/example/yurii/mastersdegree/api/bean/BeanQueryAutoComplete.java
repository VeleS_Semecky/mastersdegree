package com.example.yurii.mastersdegree.api.bean;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.example.yurii.mastersdegree.api.bean.core.Bean;
import com.example.yurii.mastersdegree.api.deps.DaggerQueryAutoCompleteDI;
import com.example.yurii.mastersdegree.api.model.queryautocomplete.Predictions;
import com.example.yurii.mastersdegree.api.model.queryautocomplete.core.HeadPlace;
import com.example.yurii.mastersdegree.api.networking.NetworkQueryAutoComplete;
import com.example.yurii.mastersdegree.api.networking.module.NetworkModule;
import com.example.yurii.mastersdegree.api.presenter.QueryAutoCompletePresenter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EBean;

import java.io.File;

@EBean
public class BeanQueryAutoComplete extends Bean<HeadPlace,NetworkQueryAutoComplete> {

    @Override
    public MutableLiveData<HeadPlace> getThLiveData() {
        return super.getThLiveData();
    }

    @AfterViews
    void init() {
        DaggerQueryAutoCompleteDI.builder().networkModule(
                new NetworkModule(
                        new File(
                                baseActivity.getCacheDir(),
                                "responses"),"https://maps.googleapis.com")).build().inject(this);
    }

    @AfterViews
    void getSong() {
        new QueryAutoCompletePresenter(networkInject, this).getQueryAutoComplete("Жоп");
    }

    public void initGetSong(String t) {
        new QueryAutoCompletePresenter(networkInject, this).getQueryAutoComplete(t);
    }


    @Override
    public void onSuccess(HeadPlace headPlace) {
        Log.i("GetDescription", headPlace.getStatus());
        thLiveData.setValue(headPlace);

        for (Predictions predictions :
                headPlace.getPredictions()) {
            Log.i("GetDescription", predictions.getDescription());
        }
    }


}
