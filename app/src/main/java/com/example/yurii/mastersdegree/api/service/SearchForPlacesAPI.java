package com.example.yurii.mastersdegree.api.service;

import com.example.yurii.mastersdegree.api.model.RouteResponse;
import com.example.yurii.mastersdegree.api.model.searchforplaces.core.SearchForPlaces;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SearchForPlacesAPI {
    @GET("/geocoding/v5/{mode}/{query}.json")
    Observable<SearchForPlaces> getSearchForPlaces(
            @Path("mode") String mode //mapbox.places
            , @Path("query") String query
            , @Query(value = "access_token") String key//pk.eyJ1IjoidmVsZXNzZW1lY2t5IiwiYSI6ImNqaW5sYmNlaTBnN2kzdm1oZmFkZWNkcWwifQ.w6iZpP4GPvw6efKXiz3f5Q
    );
}
