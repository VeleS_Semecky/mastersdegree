package com.example.yurii.mastersdegree.ui.fragment;

import android.Manifest;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.pm.PackageManager;
import android.location.Location;

import com.example.searchautocomplete.adapter.model.ObGJer;
import com.example.searchautocomplete.searchautocomplete.IOnQueryTextListener;
import com.example.searchautocomplete.searchautocomplete.SearchAutoCompleteRecyclerView;
import com.example.yurii.mastersdegree.api.bean.BeanQueryAutoComplete;
import com.example.yurii.mastersdegree.api.bean.BeanSearchForPlaces;
import com.example.yurii.mastersdegree.api.model.searchforplaces.Feature;
import com.example.yurii.mastersdegree.api.model.searchforplaces.core.SearchForPlaces;
import com.example.yurii.mastersdegree.maps.ILocationCallback;


import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.yurii.mastersdegree.R;
import com.example.yurii.mastersdegree.maps.LocationCallback;
import com.example.yurii.mastersdegree.ui.fragment.core.BaseFragment;
import com.example.yurii.mastersdegree.ui.navigator.core.ResourceManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Юрий on 24.03.2018.
 */
@EFragment(R.layout.fragment_player)
public class FirstFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleMap.OnMapLongClickListener, GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener, ILocationCallback
        , IOnQueryTextListener{
//    @Bean
//    protected BeanRouteResponsePresenter songBeanPresenter;

    @Bean
    protected BeanQueryAutoComplete beanQueryAutoComplete;
    @Bean
    protected BeanSearchForPlaces beanSearchForPlaces;

    @InstanceState
    protected Bundle onMyInstanceState;

    private GoogleMap mMap;

    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mInitLocationCallback;
    private SearchView.SearchAutoComplete mSearchAutoComplete;

//    @Bean
//    ProviderBeanField providerBeanField;

    Observer<SearchForPlaces> headPlaceObserver;

    @AfterViews
    protected void initSVM() {

        navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.SIMPLE, getBaseActivity().getResources().getText(R.string.app_name));
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getBaseActivity());
        mInitLocationCallback = LocationCallback.init(this);
        MapFragment mSupportMapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mSupportMapFragment.onCreate(onMyInstanceState);
        mSupportMapFragment.getMapAsync(this);
//        songBeanPresenter.getThLiveData().observe(getViewLifecycleOwner(), (RouteResponse routeResponse) -> {
//            List<LatLng> m_path;
//            if (routeResponse != null) {
//                m_path = PolyUtil.decode(routeResponse.getPoints());
//                PolylineOptions polyOptions = new PolylineOptions().addAll(m_path);
//                mMap.addPolyline(polyOptions);
//                LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                for (LatLng coord : m_path) {
//                    builder.include(coord);
//                }
//                LatLngBounds m_bounds = builder.build();
////                if(mMap!=null)
////                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(m_bounds, 10));
//            }
//        });

        getBaseActivity().setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        Objects.requireNonNull(getBaseActivity().getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
    }
    @Bean
    protected SearchAutoCompleteRecyclerView searchAutoCompleteRecyclerView;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menus, menu);
        MenuItem mSearch = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) mSearch.getActionView();
        searchAutoCompleteRecyclerView.init(searchView);
        searchAutoCompleteRecyclerView.setSetting();
        searchAutoCompleteRecyclerView.setOnQueryTextListener(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(15 * 1000); // two minute interval
        mLocationRequest.setFastestInterval(10 * 1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(getBaseActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //Location Permission already granted
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mInitLocationCallback, Looper.myLooper());
            mMap.setMyLocationEnabled(true);
        } else {
            //Request Location Permission
            checkLocationPermission();
        }

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if (mLastLocation != null) {
            double mDistance = SphericalUtil.computeDistanceBetween(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), latLng);
            Toast.makeText(getBaseActivity(), "Distance " + mDistance + " ", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }


    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mInitLocationCallback);
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getBaseActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getBaseActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(getBaseActivity())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", (dialogInterface, i) -> {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(getBaseActivity(),
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getBaseActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getBaseActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mInitLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getBaseActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onLocationResult(LocationResult locationResult) {
        List<Location> locationList = locationResult.getLocations();
        if (locationList.size() > 0) {
            //The last location in the list is the newest
            Location location = locationList.get(locationList.size() - 1);
            Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
            mLastLocation = location;
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }
            //Place current location marker
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title("Current Position");
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
            mCurrLocationMarker = mMap.addMarker(markerOptions);

            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
    }


    @Override
    public boolean onIQueryTextSubmit(String query) {
        Log.i("Query", "onIQueryTextSubmit: " + query);
        return false;
    }

    MutableLiveData<SearchForPlaces> thLiveData;

    @Override
    public boolean onIQueryTextChange(String newText) {
        if(thLiveData!=null) if (thLiveData.hasActiveObservers()) thLiveData.removeObserver(headPlaceObserver);
        if (newText.length() > 0) {
            thLiveData = beanSearchForPlaces.getThLiveData();
            Log.i("Query", "onIQueryTextChange: " + newText);
            beanSearchForPlaces.initGetSong(newText);
            thLiveData.observeForever(headPlaceObserver = headPlace -> {
                if (headPlace != null) {
                    if(headPlace.getFeatures().size()>0)searchAutoCompleteRecyclerView.offProgressBar();
                    searchAutoCompleteRecyclerView.getProviderBeanField().setItem(toSearchView(headPlace.getFeatures()));
                    if (thLiveData.hasActiveObservers()) thLiveData.removeObserver(headPlaceObserver);
                }
            });
        }
        return false;
    }


    private List<ObGJer> toSearchView(List<Feature> t) {
        List<ObGJer> forSearchView = new ArrayList<>();
        t.forEach(p -> {
            forSearchView.add(new ObGJer(p.getText(), p.getText()));
        });
        return forSearchView;
    }
}
