package com.example.yurii.mastersdegree.api.presenter;

import com.example.yurii.mastersdegree.api.core.BaseView;
import com.example.yurii.mastersdegree.api.model.queryautocomplete.core.HeadPlace;
import com.example.yurii.mastersdegree.api.model.searchforplaces.core.SearchForPlaces;
import com.example.yurii.mastersdegree.api.networking.NetworkQueryAutoComplete;
import com.example.yurii.mastersdegree.api.networking.NetworkSearchForPlaces;
import com.example.yurii.mastersdegree.api.networking.core.Service;
import com.example.yurii.mastersdegree.api.networking.error.NetworkError;
import com.example.yurii.mastersdegree.api.presenter.core.Presenter;

public class SearchForPlacesPresenter extends Presenter<NetworkSearchForPlaces,SearchForPlaces> {

    public SearchForPlacesPresenter(NetworkSearchForPlaces service, BaseView<SearchForPlaces> searchForPlacesBaseView) {
        super(service, searchForPlacesBaseView);
    }
    public void getSearchForPlaces(String mode, String query, String key){
        yBaseView.showWait();
        subscription.add(service.getSearchForPlaces(mode, query, key ,new Service.Callback<SearchForPlaces>() {
            @Override
            public void onSuccess(SearchForPlaces t) {
                yBaseView.closeWait();
                yBaseView.onSuccess(t);
                onStop();
            }

            @Override
            public void onError(NetworkError networkError) {
                yBaseView.closeWait();
                yBaseView.onFailed(networkError.getAppErrorMessage());//
                onStop();
            }
        }));
        super.subscription.add(subscription);
    }
}
