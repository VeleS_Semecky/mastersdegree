package com.example.yurii.mastersdegree.api.deps;

import com.example.yurii.mastersdegree.api.bean.BeanSearchForPlaces;
import com.example.yurii.mastersdegree.api.networking.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {NetworkModule.class})
public interface SearchForPlacesDI {
    void inject(BeanSearchForPlaces t);
}
