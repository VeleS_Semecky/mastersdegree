package com.example.yurii.mastersdegree.api.service;

import com.example.yurii.mastersdegree.api.model.RouteResponse;
import com.example.yurii.mastersdegree.api.model.queryautocomplete.core.HeadPlace;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface QueryAutoCompleteAPI {
    @GET("/maps/api/place/queryautocomplete/json")
    Observable<HeadPlace> getHeadPlace(
            @Query(value = "input") String input
            , @Query(value = "key") String key
    );
}
