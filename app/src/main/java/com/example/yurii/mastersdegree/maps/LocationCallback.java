package com.example.yurii.mastersdegree.maps;

import com.google.android.gms.location.LocationResult;

public class LocationCallback extends com.google.android.gms.location.LocationCallback {
    private static ILocationCallback iLocationCallback;

    public static LocationCallback init(ILocationCallback iLocationCallback){
        LocationCallback.iLocationCallback = iLocationCallback;
        return new LocationCallback();
    }

    @Override
    public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        iLocationCallback.onLocationResult(locationResult);
    }
}
