package com.example.searchautocomplete.adapter.bean;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.example.searchautocomplete.R;
import com.example.searchautocomplete.adapter.model.ObGJer;
import com.example.searchautocomplete.adapter.track.Adapter;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

import java.util.List;

@EBean
public class ProviderBeanField {

    @RootContext
    protected Context baseActivity;

    @Bean
    protected Adapter adapter;

    @ViewById(resName="recycler_view")
    protected RecyclerView recyclerView;

    @AfterViews
    void initTrackAdapter(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(baseActivity));
        recyclerView.setAdapter(adapter);
    }

    private boolean isFirstSearch = true;

    public void setItem(List<ObGJer> t){
        if (isFirstSearch) {
            recyclerView.setAnimation(AnimationUtils.loadAnimation(baseActivity, R.anim.slid));
        }
        isFirstSearch = false;
        adapter.setItem(t);
        adapter.notifyDataSetChanged();
    }
    public void clearAllItem(){
        adapter.getItem().clear();
        adapter.notifyDataSetChanged();
        isFirstSearch = true;
    }

}
