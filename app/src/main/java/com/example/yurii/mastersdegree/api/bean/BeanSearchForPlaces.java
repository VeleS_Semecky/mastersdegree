package com.example.yurii.mastersdegree.api.bean;

import android.arch.lifecycle.MutableLiveData;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.example.yurii.mastersdegree.api.bean.core.Bean;
import com.example.yurii.mastersdegree.api.deps.DaggerSearchForPlacesDI;
import com.example.yurii.mastersdegree.api.model.searchforplaces.core.SearchForPlaces;
import com.example.yurii.mastersdegree.api.networking.NetworkSearchForPlaces;
import com.example.yurii.mastersdegree.api.networking.module.NetworkModule;
import com.example.yurii.mastersdegree.api.presenter.SearchForPlacesPresenter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EBean;

import java.io.File;
@EBean
public class BeanSearchForPlaces extends Bean<SearchForPlaces,NetworkSearchForPlaces> {

    @Override
    public MutableLiveData<SearchForPlaces> getThLiveData() {
        return super.getThLiveData();
    }

    @AfterViews
    void init() {
        DaggerSearchForPlacesDI.builder().networkModule(
                new NetworkModule(
                        new File(
                                baseActivity.getCacheDir(),
                                "responses"),"https://api.mapbox.com")).build().inject(this);
    }

    @AfterViews
    void getPresenter() {
        new SearchForPlacesPresenter(networkInject, this).getSearchForPlaces("","Оти","");
    }

    public void initGetSong(String p) {
        new SearchForPlacesPresenter(networkInject, this).getSearchForPlaces("",p,"");
    }

    @Override
    public void onSuccess(SearchForPlaces t) {
        super.onSuccess(t);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            t.getFeatures().forEach(feature ->{
                Log.i("GetSearchForPlaces", feature.getText());
                Toast.makeText(baseActivity,  feature.getText(), Toast.LENGTH_SHORT).show();
            });
        }

    }

}