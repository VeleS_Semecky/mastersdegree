package com.example.searchautocomplete.adapter.track;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.searchautocomplete.adapter.model.ObGJer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(resName="item")
public class ItemView extends LinearLayout {

    Context myContext;

    @ViewById(resName="nameFields")
    TextView trArtist;

    ObGJer fieldTable;


    @AfterViews
    public void init() {
    }

    @Click(resName="nameFields")
    public void OnClick() {
Toast.makeText(myContext,trArtist.getText(),Toast.LENGTH_SHORT).show();
        //        progressDialogInMyApp.showDialog();

    }
//    @LongClick(R.id.itemTrackInList)
//    public void OnLongClick(){
//        trackExoFinder.seekTo(null);
//    }

    public ItemView(Context context) {
        super(context);
        myContext = context;
        this.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
//        mRoomViewModel =  ViewModelProviders.of((FragmentActivity) context).get(RoomViewModel.class);

    }

    public void bind(ObGJer track) {
        fieldTable = track;
        trArtist.setText(track.getField());
//        progressDialogInMyApp = new ProgressDialogInMyApp((FragmentActivity) myContext,fieldTable.getNameField(),1);

    }


}
