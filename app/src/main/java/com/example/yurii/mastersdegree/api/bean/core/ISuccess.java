package com.example.yurii.mastersdegree.api.bean.core;

public interface ISuccess<T> {
    void onSuccess(T t);
}
