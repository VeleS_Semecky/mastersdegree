package com.example.yurii.mastersdegree.api.presenter;


import com.example.yurii.mastersdegree.api.core.BaseView;
import com.example.yurii.mastersdegree.api.model.RouteResponse;
import com.example.yurii.mastersdegree.api.networking.NetworkSong;
import com.example.yurii.mastersdegree.api.networking.core.Service;
import com.example.yurii.mastersdegree.api.networking.error.NetworkError;
import com.example.yurii.mastersdegree.api.presenter.core.Presenter;

public class SongPresenter extends Presenter<NetworkSong,RouteResponse> {
    public SongPresenter(NetworkSong service, BaseView<RouteResponse> songBaseView) {
        super(service, songBaseView);
    }


    public void getRoute(){
        yBaseView.showWait();
        subscription.add(service.getRoute(new Service.Callback<RouteResponse>() {
            @Override
            public void onSuccess(RouteResponse song) {
                yBaseView.closeWait();
                yBaseView.onSuccess(song);
                onStop();
            }

            @Override
            public void onError(NetworkError networkError) {
                yBaseView.closeWait();
                yBaseView.onFailed(networkError.getAppErrorMessage());//
                onStop();
            }
        }));
        super.subscription.add(subscription);
    }
}
