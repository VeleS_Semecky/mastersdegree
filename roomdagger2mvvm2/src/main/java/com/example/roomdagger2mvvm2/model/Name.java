package com.example.roomdagger2mvvm2.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.example.roomdagger2mvvm2.constant.RoomConstant;

@Entity(tableName = RoomConstant.TableNameRoomDatabase.TABLE_NAMES)
public class Name {

    @PrimaryKey
//            (autoGenerate = true)
    private int id ;

    @ColumnInfo(name = "name")
    private String thName;



    public Name(int id, String thName) {
        this.id = id;
        this.thName = thName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThName() {
        return thName;
    }

    public void setThName(String thName) {
        this.thName = thName;
    }
}
