package com.example.roomdagger2mvvm2.constant;


import android.arch.persistence.room.OnConflictStrategy;

public interface RoomConstant {
    /**
     * @param NAME_ROOM_DATABASE    - this is name database;
     *
     * @param VERSION_ROOM_DATABASE - version database;
     *
     * @param onConflictDaoInsert   - set of conflict handling strategies for various Dao methods;
     *
     * @param TableNameRoomDatabase - interface with name table in database;
     */
    String NAME_ROOM_DATABASE = "demo-db";

    int VERSION_ROOM_DATABASE = 1;

    int onConflictDaoInsert = OnConflictStrategy.IGNORE;

    interface TableNameRoomDatabase {

       String TABLE_NAMES = "name_table";
    }
}
